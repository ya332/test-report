# Test report in merge request

> - [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/45318) in GitLab 11.2.
> - [Feature flag enabled by default](https://gitlab.com/gitlab-org/gitlab/-/issues/216478) in GitLab 13.3.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/216478) in GitLab 13.3.

This project demonstrates how to implement test reports with [GitLab CI/CD](https://docs.gitlab.com/ee/ci/README.html).

You can configure your job to use Unit test reports, and GitLab will display a report on the merge request so that it’s easier and faster to identify the failure without having to check the entire log. Unit test reports currently only support test reports in the JUnit report format.

Check [.gitlab-ci.yml](/.gitlab-ci.yml) to understand how to set up the feature.

## Demo

This demo implementation uses different languages.

There are sample merge requests that show the feature in action:

- [Merge request #2](https://gitlab.com/gitlab-org/ci-cd/demos/test-report/-/merge_requests/2) shows a test report for JavaScript with **jest**.
- [Test results on pipeline](https://gitlab.com/gitlab-org/ci-sample-projects/test-report/-/pipelines/569328295/test_report) a direct link to test report, pipeline level
---

Read more about the feature on [GitLab documentation](https://docs.gitlab.com/ee/ci/unit_test_reports.html).
